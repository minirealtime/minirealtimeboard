package com.realtimeboard.mini.entity;

public class AreaRequest {

    private double startX;
    private double startY;
    private double width;
    private double height;

    public AreaRequest(){

    }

    public AreaRequest(double startX, double startY, double width, double height) {
        this.startX = startX;
        this.startY = startY;
        this.width = width;
        this.height = height;
    }

    public double getStartX() {
        return startX;
    }

    public void setStartX(double startX) {
        this.startX = startX;
    }

    public double getStartY() {
        return startY;
    }

    public void setStartY(double startY) {
        this.startY = startY;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    //если высота или ширина равны нулю - значит, фильтр по области на экране не задан
    public boolean isEmpty(){
        return width==0 || height==0;
    }
}
