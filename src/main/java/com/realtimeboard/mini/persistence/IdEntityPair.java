package com.realtimeboard.mini.persistence;

public interface IdEntityPair<Id, Entity> {
    Id getId();

    Entity getEntity();
}
