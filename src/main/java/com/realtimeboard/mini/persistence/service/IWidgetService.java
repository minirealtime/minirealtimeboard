package com.realtimeboard.mini.persistence.service;

import com.realtimeboard.mini.entity.AreaRequest;
import com.realtimeboard.mini.entity.Widget;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IWidgetService {

    Widget findOne(final long id);

    List<Widget> findAll();

    Widget create(final Widget entity);

    Widget update(final Widget entity);

    void deleteById(final long id);

    Page<Widget> getAll(int pageNumber, int pageSize, AreaRequest areaRequest);
}
