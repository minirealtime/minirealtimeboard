package com.realtimeboard.mini.persistence.service;

import com.realtimeboard.mini.entity.AreaRequest;
import com.realtimeboard.mini.entity.Widget;
import com.realtimeboard.mini.persistence.ConcurrentHashMapDataStore;
import com.realtimeboard.mini.persistence.repo.WidgetRepository;
import com.realtimeboard.mini.persistence.repo.MemoryRepositoryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class WidgetService implements IWidgetService {

    private WidgetRepository repo;

    public WidgetService() {
        ConcurrentHashMapDataStore<Long, Widget> dataStore = new ConcurrentHashMapDataStore<Long, Widget>();
        MemoryRepositoryFactory<Widget, Long> repositoryFactory = new MemoryRepositoryFactory<Widget, Long>(dataStore);
        repo = repositoryFactory.getRepository(WidgetRepository.class);
    }

    @Override
    public Widget findOne(long id) {
        Optional<Widget> found = repo.findById(id);
        return found.isPresent() ? found.get() : null;
    }

    @Override
    public List<Widget> findAll() {
        Sort.Order zIndexOrder = Sort.Order.asc("zIndex");
        Sort sort = Sort.by(zIndexOrder);
        return (List<Widget>) repo.findAll(sort);
    }

    @Override
    public Widget create(Widget entity) {
        entity.setUpdatedAt(new Date());
        if (entity.getId() == null) {
            Long lastId = repo.getLastId();
            Long newId = lastId == null ? new Long(1) : new Long(lastId.longValue() + 1);
            entity.setId(newId);
        }
        return repo.save(entity);
    }

    @Override
    public Widget update(Widget entity) {
        entity.setUpdatedAt(new Date());
        return repo.save(entity);
    }

    @Override
    public void deleteById(long id) {
        repo.deleteById(id);
    }

    @Override
    public Page<Widget> getAll(int pageNumber, int pageSize, AreaRequest areaRequest) {
        Sort.Order zIndexOrder = Sort.Order.asc("zIndex");
        Sort sort = Sort.by(zIndexOrder);
        Pageable pageableRequest = PageRequest.of(pageNumber -1,pageSize, sort);
        return repo.findAllInArea(pageableRequest, areaRequest);
    }
}
