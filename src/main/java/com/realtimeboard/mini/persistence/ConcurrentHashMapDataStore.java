package com.realtimeboard.mini.persistence;

import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapDataStore<Id, Entity> implements DataStore<Id, Entity> {

    private final ConcurrentHashMap<Id, Entity> map = new ConcurrentHashMap<Id, Entity>();

    @Override
    public Object getDelegate() {
        return map;
    }

    @Override
    public Id getLastEntryId() {
        TreeMap<Id, Entity> tempMap = new TreeMap<Id, Entity>();
        tempMap.putAll(this.map);
        return tempMap.size() > 0 ? tempMap.lastKey() : null;
    }

    @Override
    public Iterator<IdEntityPair<Id, Entity>> iterator() {
        final Iterator<Map.Entry<Id, Entity>> iter = map.entrySet().iterator();
        return new Iterator<IdEntityPair<Id, Entity>>() {
            @Override
            public boolean hasNext() {
                return iter.hasNext();
            }

            @Override
            public IdEntityPair<Id, Entity> next() {
                return new MapEntryIdEntityPairAdapter<Id, Entity>(iter.next());
            }

            @Override
            public void remove() {
                iter.remove();
            }
        };
    }

    static class MapEntryIdEntityPairAdapter<Id, Entity> implements IdEntityPair<Id, Entity> {

        private final Map.Entry<Id, Entity> entry;

        public MapEntryIdEntityPairAdapter(Map.Entry<Id, Entity> entry) {
            this.entry = entry;
        }

        @Override
        public Id getId() {
            return entry.getKey();
        }

        @Override
        public Entity getEntity() {
            return entry.getValue();
        }

    }
}
