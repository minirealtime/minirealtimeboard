package com.realtimeboard.mini.persistence;

public interface DataStore<Id, T> extends Iterable<IdEntityPair<Id,T>>{
    Object getDelegate();
    Id getLastEntryId();
}
