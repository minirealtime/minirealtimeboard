package com.realtimeboard.mini.persistence.repo;

import com.realtimeboard.mini.persistence.DataStore;
import com.realtimeboard.mini.persistence.IdEntityPair;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.repository.core.EntityInformation;
import org.springframework.data.repository.core.NamedQueries;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.ReflectionEntityInformation;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;
import org.springframework.data.repository.query.QueryLookupStrategy;
import org.springframework.data.repository.query.QueryMethod;
import org.springframework.data.repository.query.QueryMethodEvaluationContextProvider;
import org.springframework.data.repository.query.RepositoryQuery;
import org.springframework.data.repository.query.parser.Part;
import org.springframework.data.repository.query.parser.PartTree;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class MemoryRepositoryFactory<T, Id> extends RepositoryFactorySupport {

    private final DataStore<Id, T> dataStore;

    public MemoryRepositoryFactory(DataStore<Id, T> dataStore) {
        this.dataStore = dataStore;
    }

    @Override
    public <T, Id> EntityInformation<T, Id> getEntityInformation(Class<T> aClass) {
        return new ReflectionEntityInformation<T, Id>(aClass);
    }

    @Override
    protected Object getTargetRepository(RepositoryInformation repositoryInformation) {
        EntityInformation<T, Id> entityInformation = this.<T, Id>getEntityInformation((Class<T>) repositoryInformation.getDomainType());
        return new MemoryRepositoryImpl<T, Id>(dataStore, entityInformation);
    }

    @Override
    protected Class<?> getRepositoryBaseClass(RepositoryMetadata repositoryMetadata) {
        return MemoryRepositoryImpl.class;
    }

    @Override
    protected Optional<QueryLookupStrategy> getQueryLookupStrategy(QueryLookupStrategy.Key key, QueryMethodEvaluationContextProvider evaluationContextProvider) {
        return Optional.of(new InMemoryQueryLookupStrategy<T, Id>(key, evaluationContextProvider, dataStore));
    }

    public static class InMemoryQueryLookupStrategy<T, Id> implements QueryLookupStrategy {

        private Key key;
        private QueryMethodEvaluationContextProvider evaluationContextProvider;
        private DataStore<Id, T> dataStore;

        public InMemoryQueryLookupStrategy(Key key, QueryMethodEvaluationContextProvider evaluationContextProvider, DataStore<Id, T> dataStore) {
            this.key = key;
            this.evaluationContextProvider = evaluationContextProvider;
            this.dataStore = dataStore;
        }

        @Override
        public RepositoryQuery resolveQuery(Method method, RepositoryMetadata repositoryMetadata, ProjectionFactory projectionFactory, NamedQueries namedQueries) {
            QueryMethod queryMethod = new QueryMethod(method, repositoryMetadata, projectionFactory);
            return new InMemoryPartTreeQuery<T, Id>(queryMethod, evaluationContextProvider, dataStore);

        }
    }

    public static class InMemoryPartTreeQuery<T, Id> implements RepositoryQuery {
        private QueryMethod queryMethod;
        private Expression expression;
        private QueryMethodEvaluationContextProvider evaluationContextProvider;
        private DataStore<Id, T> dataStore;

        public InMemoryPartTreeQuery(QueryMethod queryMethod, QueryMethodEvaluationContextProvider evaluationContextProvider,
                                     DataStore<Id, T> dataStore) {
            this.queryMethod = queryMethod;
            this.evaluationContextProvider = evaluationContextProvider;
            this.expression = toPredicateExpression(queryMethod);
            this.dataStore = dataStore;
        }

        protected Expression toPredicateExpression(QueryMethod queryMethod) {
            System.out.println(queryMethod.getName());
            PartTree tree = new PartTree(queryMethod.getName(), queryMethod.getEntityInformation().getJavaType());

            int parameterIndex = 0;
            StringBuilder sb = new StringBuilder();
            for (Iterator<PartTree.OrPart> orPartIter = tree.iterator(); orPartIter.hasNext(); ) {
                PartTree.OrPart orPart = orPartIter.next();

                int partCnt = 0;
                StringBuilder partBuilder = new StringBuilder();

                for (Iterator<Part> partIter = orPart.iterator(); partIter.hasNext(); ) {
                    Part part = partIter.next();
                    partBuilder.append("#it?.");
                    partBuilder.append(part.getProperty().toDotPath().replace(".", ".?")).append("? equals (").append("[")
                            .append(parameterIndex++).append("])");
                    if (partIter.hasNext()) {
                        partBuilder.append("&&");
                    }

                    partCnt++;
                }

                if (partCnt > 1) {
                    sb.append("(").append(partBuilder).append(")");
                } else {
                    sb.append(partBuilder);
                }

                if (orPartIter.hasNext()) {
                    sb.append("||");
                }
            }

            return new SpelExpressionParser().parseExpression(sb.toString());
        }

        @Override
        public Object execute(Object[] parameters) {
            List<Object> resultList = new ArrayList<Object>();

            EvaluationContext ec = new StandardEvaluationContext(parameters);
            for (IdEntityPair<Id, T> pair : this.dataStore) {
                ec.setVariable("it", pair.getEntity());
                Object test = expression.getValue(ec);

                if (Boolean.TRUE.equals(test)) {
                    resultList.add(pair.getEntity());
                }
            }

            return resultList;
        }

        @Override
        public QueryMethod getQueryMethod() {
            return queryMethod;
        }
    }
}
