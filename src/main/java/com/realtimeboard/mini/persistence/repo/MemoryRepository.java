package com.realtimeboard.mini.persistence.repo;

import com.realtimeboard.mini.entity.AreaRequest;
import com.realtimeboard.mini.entity.Widget;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MemoryRepository<T, Id> extends CrudRepository<T, Id>, PagingAndSortingRepository<T, Id> {
    Id getLastId();
    Page<T> findAllInArea(Pageable pageable, AreaRequest request);
}
