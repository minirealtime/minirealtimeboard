package com.realtimeboard.mini.persistence.repo;

import com.realtimeboard.mini.entity.AreaRequest;
import com.realtimeboard.mini.entity.Widget;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface WidgetRepository extends MemoryRepository<Widget, Long> {
}
