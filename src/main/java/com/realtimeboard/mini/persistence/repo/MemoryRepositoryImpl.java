package com.realtimeboard.mini.persistence.repo;

import com.realtimeboard.mini.entity.AreaRequest;
import com.realtimeboard.mini.entity.Widget;
import com.realtimeboard.mini.persistence.DataStore;
import org.springframework.core.GenericTypeResolver;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.core.EntityInformation;
import org.springframework.util.Assert;

import java.lang.reflect.Field;
import java.util.*;

public class MemoryRepositoryImpl<T, Id> implements MemoryRepository<T, Id>, CrudRepository<T, Id>, PagingAndSortingRepository<T, Id> {

    private final EntityInformation<T, ?> entityInformation;
    private final DataStore<Id, T> dataStore;

    public MemoryRepositoryImpl(DataStore<Id, T> dataStore, EntityInformation<T, ?> entityInformation) {
        this.dataStore = dataStore;
        this.entityInformation = entityInformation;
    }


    @Override
    public <S extends T> S save(S entity) {
        Id id = extractId(entity);
        Assert.notNull(id, "ID виджета не может быть пустым");
        if (getStore().get(id) == null) {
            getStore().put(id, entity);
        } else {
            getStore().replace(id, entity);
        }
        return entity;
    }

    @Override
    public <S extends T> List<S> saveAll(Iterable<S> entities) {
        List<S> result = new ArrayList<S>();
        for (S entity : entities) {
            result.add(save(entity));
        }
        return result;
    }

    @Override
    public Optional<T> findById(Id id) {
        return Optional.ofNullable(getStore().get(id));
    }

    @Override
    public boolean existsById(Id id) {
        return getStore().containsKey(id);
    }

    @Override
    public List<T> findAll() {
        return new ArrayList<T>(getStore().values());
    }

    @Override
    public List<T> findAllById(Iterable<Id> ids) {
        List<T> values = new ArrayList<T>();

        Map<Id, T> map = getStore();
        for (Id id : ids) {
            T value = map.get(id);
            if (value != null) {
                values.add(value);
            }
        }
        return values;
    }

    @Override
    public long count() {
        return getStore().size();
    }

    @Override
    public void deleteById(Id id) {
        getStore().remove(id);
    }

    @Override
    public void delete(T entity) {
        Id id = extractId(entity);
        if (id != null) {
            this.deleteById(id);
        }
    }

    @Override
    public void deleteAll(Iterable<? extends T> entities) {
        for (T value : entities) {
            delete(value);
        }
    }

    @Override
    public void deleteAll() {
        getStore().clear();
    }


    private Map<Id, T> getStore() {
        return (Map<Id, T>) dataStore.getDelegate();
    }

    private Id extractId(T entity) {
        return (Id) entityInformation.getId(entity);
    }

    @Override
    public Id getLastId() {
        return dataStore.getLastEntryId();
    }

    @Override
    public Page<T> findAllInArea(Pageable pageable, AreaRequest request) {
        Sort sort = pageable.getSort();
        Iterable<T> all = findAll(sort);
        Iterator<T> allIt = all.iterator();
        List<T> allItems = new ArrayList<T>();
        allIt.forEachRemaining(allItems::add);
        List<T> areaItems = new ArrayList<T>();
        List<T> pageItems = new ArrayList<T>();
        //если тип данных имеет координаты (есть поля x, y) - фильтруем по области, иначе - отдаём просто страницу без фильтра
        try {
            if(request.isEmpty()) {
                areaItems = allItems;
            } else {
                //выбираем все элементы в области
                for (int i = 0; i < allItems.size(); i++) {
                    T current = allItems.get(i);
                    double x = (double) runGetter(current,"x");
                    double y = (double) runGetter(current, "y");
                    if (x > request.getStartX()
                            && x < request.getStartX() + request.getWidth()
                            && y > request.getStartY()
                            && y < request.getHeight()) {
                        areaItems.add(current);
                    }
                }
            }

        } catch (NoSuchFieldException e) {
            areaItems = allItems;
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            areaItems = allItems;
            e.printStackTrace();
        }
        for(int i=(int)pageable.getOffset();i < pageable.getOffset() + pageable.getPageSize() && i<areaItems.size(); i++) {
            pageItems.add(areaItems.get(i));
        }
        return new PageImpl<T>(pageItems, pageable, areaItems.size());
    }

    @Override
    public Iterable<T> findAll(Sort sort) {
        List<Map.Entry<Id, T>> tempList = new ArrayList<>(getStore().entrySet());
        tempList.sort(new ValueComparator(sort));
        List<T> result = new ArrayList<T>();
        for(Map.Entry<Id, T> entry: tempList)
            result.add(entry.getValue());
        return result;
    }

    @Override
    public Page<T> findAll(Pageable pageable) {
        Sort sort = pageable.getSort();
        Iterable<T> all = findAll(sort);
        Iterator<T> allIt = all.iterator();
        List<T> allItems = new ArrayList<T>();
        allIt.forEachRemaining(allItems::add);
        List<T> pageItems = new ArrayList<T>();
        for(int i=(int)pageable.getOffset(); i < pageable.getOffset()+pageable.getPageSize() && i < allItems.size(); i++ ) {
            pageItems.add(allItems.get(i));
        }
        return new PageImpl<T>(pageItems, pageable, allItems.size());
    }

    private Object runGetter(Object obj, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        Field field = obj.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return field.get(obj);
    }

    class ValueComparator implements  Comparator<Map.Entry<Id, T>> {

        private Sort sort;

        public ValueComparator(Sort sort) {
            this.sort = sort;
        }

        @Override
        public int compare(Map.Entry<Id, T> entry1, Map.Entry<Id, T> entry2) {
            int compareResult = 0;
            T value1 = entry1.getValue();
            T value2 = entry2.getValue();
            try {
                for (Sort.Order order : sort) {
                    Object field1 = runGetter(value1, order.getProperty());
                    Object field2 = runGetter(value2, order.getProperty());
                    boolean isAcending = order.getDirection().isAscending();
                    if(field1 instanceof Comparable) {
                        compareResult = ((Comparable)field1).compareTo(field2);
                        if(!isAcending)
                            compareResult = -compareResult;
                    }
                    if(compareResult != 0)
                        break;
                }
            }catch (Exception ex) {
                ex.printStackTrace();
            }
            return compareResult;
        }

    }
}
