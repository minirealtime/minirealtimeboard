package com.realtimeboard.mini.controller;

import com.realtimeboard.mini.entity.AreaRequest;
import com.realtimeboard.mini.entity.Widget;
import com.realtimeboard.mini.persistence.service.IWidgetService;
import com.realtimeboard.mini.persistence.service.WidgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/widget")
class WidgetController {

    private WidgetService service;

    public WidgetController() {
        super();
        service = new WidgetService();
    }

    @GetMapping(value = "/get")
    public ResponseEntity<Widget> getWidgetById(@RequestParam("id") long id) {
        Widget result = service.findOne(id);
        if (result == null)
            return new ResponseEntity<>(null, HttpStatus.OK);
        else
            return new ResponseEntity<Widget>(result, HttpStatus.OK);
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Widget> createWidget(@RequestBody Widget widget) {
        return new ResponseEntity<Widget>(service.create(widget), HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<Widget> updateWidget(@RequestBody Widget widget) {
        return new ResponseEntity<Widget>(service.create(widget), HttpStatus.OK);
    }

    @GetMapping(value = "/delete")
    public ResponseEntity<Boolean> deleteWidgetById(@RequestParam("id") long id) {
        service.deleteById(id);
        return new ResponseEntity<Boolean>(true, HttpStatus.OK);
    }

    @GetMapping(value="/getAll")
    public ResponseEntity<Page<Widget>> getAll(@RequestParam("page")int pageNumber,
                                               @RequestParam(value = "limit", required = false, defaultValue = "10") int pageSize,
                                               @RequestParam(value="areaX", required = false, defaultValue = "0") double areaX,
                                               @RequestParam(value="areaY", required = false, defaultValue = "0") double areaY,
                                               @RequestParam(value="areaWidth", required = false, defaultValue = "0") double areaWidth,
                                               @RequestParam(value="areaHeight", required = false, defaultValue = "0") double areaHeight ) {
        AreaRequest areaRequest = new AreaRequest(areaX, areaY,areaWidth,areaHeight);
        return new ResponseEntity<Page<Widget>>(service.getAll(pageNumber, pageSize, areaRequest), HttpStatus.OK);
    }

}
