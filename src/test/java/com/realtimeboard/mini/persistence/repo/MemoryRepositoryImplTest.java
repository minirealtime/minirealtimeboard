package com.realtimeboard.mini.persistence.repo;

import com.realtimeboard.mini.entity.Widget;
import com.realtimeboard.mini.persistence.ConcurrentHashMapDataStore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MemoryRepositoryImplTest {

    private MemoryRepository<Widget, Long> repo;

    private void initRepo() {
        ConcurrentHashMapDataStore<Long, Widget> dataStore = new ConcurrentHashMapDataStore<Long, Widget>();
        MemoryRepositoryFactory<Widget, Long> repositoryFactory = new MemoryRepositoryFactory<Widget, Long>(dataStore);
        repo = repositoryFactory.getRepository(WidgetRepository.class);
    }

    @Test
    public void save() {
        initRepo();
        Widget widget = createWidget(0,0,100,100,1, 1l);
        repo.save(widget);
    }

    @Test
    public void saveAll() {
        initRepo();
        List<Widget> list = new ArrayList<Widget>();
        list.add(createWidget(0,0,100,100,1,1l));
        list.add(createWidget(0,0,100,100,1, 2l));
        list.add(createWidget(0,0,100,100,1, 3l));
        repo.saveAll(list);
    }

    @Test
    public void findById() {
        initRepo();
        save();
        repo.findById(1l);
    }

    @Test
    public void existsById() {
        initRepo();
        save();
        repo.existsById(1l);
    }

    @Test
    public void findAll() {
        initRepo();
        saveAll();
        repo.findAll();
    }

    @Test
    public void findAllById() {
        initRepo();
        saveAll();
        List<Long> ids = new ArrayList<Long>();
        ids.add(1l);
        ids.add(2l);
        repo.findAllById(ids);
    }

    @Test
    public void deleteById() {
        initRepo();
        save();
        repo.deleteById(1l);
        assertTrue(repo.count() == 0);
    }

    @Test
    public void delete() {
        initRepo();
        Widget widget = createWidget(0,0,100,100,1, 1l);
        repo.save(widget);
        repo.delete(widget);
        assertTrue(repo.count() == 0);
    }

    private Widget createWidget(double x, double y, double width, double height, int zIndex, long id) {
        Widget widget = new Widget();
        widget.setX(x);
        widget.setY(y);
        widget.setWidth(width);
        widget.setHeight(height);
        widget.setzIndex(zIndex);
        widget.setId(id);
        return widget;
    }
}