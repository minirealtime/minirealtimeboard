package com.realtimeboard.mini.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.realtimeboard.mini.entity.Widget;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WidgetControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getWidgetById() {
    }

    @Test
    public void createWidget() throws Exception {

        String json = createWidgetJson(0,0,100,100,1);

        this.mockMvc.perform(post("/widget/create").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json)).andExpect(status().isOk());
    }

    @Test
    public void updateWidget() throws Exception {
        Widget widget = createWidget(0,0,100,100,1);

        String json = createObjectJson(widget);

        this.mockMvc.perform(post("/widget/create").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json)).andExpect(status().isOk());
        widget.setId(1l);
        widget.setWidth(110);

        json = createObjectJson(widget);

        this.mockMvc.perform(post("/widget/update").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json)).andExpect(status().isOk());

    }

    @Test
    public void deleteWidgetById() throws Exception {
        createWidget();
        this.mockMvc.perform(get("/widget/delete").param("id","1")).andExpect(status().isOk());
    }

    @Test
    public void getAll() throws Exception {
        for(int i=0; i < 20; i++) {
            createWidget();
        }
        this.mockMvc.perform(get("/widget/getAll").param("page","1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(10))
                .andExpect(jsonPath("$.totalPages").value(2));
    }

    @Test
    public void getAllInArea() throws Exception {

        String json1 = createWidgetJson(50,50,100,100,1);
        String json2 = createWidgetJson(50, 100,100,100,1);
        String json3 = createWidgetJson(100,100,100,100,1);
        this.mockMvc.perform(post("/widget/create").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json1)).andExpect(status().isOk());
        this.mockMvc.perform(post("/widget/create").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json2)).andExpect(status().isOk());
        this.mockMvc.perform(post("/widget/create").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json3)).andExpect(status().isOk());

        this.mockMvc.perform(get("/widget/getAll")
                .param("page", "1")
                .param("areaX", "0")
                .param("areaY", "0")
                .param("areaWidth", "100")
                .param("areaHeight", "150"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(2));


    }

    private String createObjectJson(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = writer.writeValueAsString(obj);

        return requestJson;
    }

    private Widget createWidget(double x, double y, double width, double height, int zIndex) {
        Widget widget = new Widget();
        widget.setX(x);
        widget.setY(y);
        widget.setWidth(width);
        widget.setHeight(height);
        widget.setzIndex(zIndex);

        return widget;
    }

    private String createWidgetJson(double x, double y, double width, double height, int zIndex) throws JsonProcessingException {
        Widget widget = createWidget(x,y,width,height,zIndex);
        return createObjectJson(widget);
    }
}